/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <signal.h>
#include <camera/CameraMetadata.h>
#include <qmmf-sdk/qmmf_recorder.h>
#include <qmmf-sdk/qmmf_recorder_extra_param_tags.h>
#include <modal_pipe.h>

// Name spaces
using namespace qmmf;
using namespace recorder;
using namespace android;

// Constants
const int NO_ERRORS = 0;
const int ERROR     = -1;

// Class instances
Recorder _recorder;

// Global variables
bool running                 = true;
bool debug                   = false; // General debug messages
bool frame_debug             = false; // High rate frame level debug messages
int camera_number            = 0; // Camera to use (0-3)
uint32_t session_id          = 0;
uint32_t mpa_video_track_id  = 1;
uint32_t h26x_video_track_id = 2;

// This global holds the name of the file to store the compressed HD video.
// It is set by a command line option. If not set then HD video will not be
// saved to a file.
std::string video_out_file_name = std::string("");
FILE* video_file = nullptr;

// This global holds the base name of the file to store JPEG snapshots. An image number
// will be appended to this base file name to differentiate the snapshot images.
// It is set by a command line option. If not set then JPEG snapshots will not be
// saved to a file.
std::string snapshot_base_file_name = std::string("");
static uint32_t snapshot_number = 1;
bool enable_yuv_snapshot = false;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void signal_handler(int dummy) {
    running = false;
    printf("\n");
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void PackYUVImage(BufferDescriptor &buffer, MetaData &meta_data) {
#ifdef QRB5165
            uint8_t *buffer_ptr = ((uint8_t*) buffer.data);
            uint8_t *y_data_ptr = ((uint8_t*) buffer.data);
            for (int i = 0; i < meta_data.cam_buffer_meta_data.plane_info[0].height; i++) {
                memmove(buffer_ptr, y_data_ptr, meta_data.cam_buffer_meta_data.plane_info[0].width);
                buffer_ptr += meta_data.cam_buffer_meta_data.plane_info[0].width;
                y_data_ptr += meta_data.cam_buffer_meta_data.plane_info[0].stride;
            }
            uint8_t *uv_data_ptr = ((uint8_t*) buffer.data) + meta_data.cam_buffer_meta_data.plane_info[1].offset;
            for (int i = 0; i < meta_data.cam_buffer_meta_data.plane_info[1].height; i++) {
                memmove(buffer_ptr, uv_data_ptr, meta_data.cam_buffer_meta_data.plane_info[0].width);
                buffer_ptr += meta_data.cam_buffer_meta_data.plane_info[1].width;
                uv_data_ptr += meta_data.cam_buffer_meta_data.plane_info[1].stride;
            }

            buffer.size = (meta_data.cam_buffer_meta_data.plane_info[0].width * meta_data.cam_buffer_meta_data.plane_info[0].height) +
                        (meta_data.cam_buffer_meta_data.plane_info[1].width * meta_data.cam_buffer_meta_data.plane_info[1].height);
#else
            //note: this assumes width = stride !!!
            int y_plane_full_size =  meta_data.cam_buffer_meta_data.plane_info[0].stride *
                                     meta_data.cam_buffer_meta_data.plane_info[0].scanline;

            int y_plane_real_size =  meta_data.cam_buffer_meta_data.plane_info[0].stride *
                                     meta_data.cam_buffer_meta_data.plane_info[0].height;

            int uv_plane_real_size = meta_data.cam_buffer_meta_data.plane_info[1].stride *
                                     meta_data.cam_buffer_meta_data.plane_info[1].height;

            uint8_t* uv_plane_full_start = ((uint8_t*) buffer.data) + y_plane_full_size;
            uint8_t* uv_plane_new_start  = ((uint8_t*) buffer.data) + y_plane_real_size;

            //only move memory if necessary
            if (uv_plane_full_start != uv_plane_new_start){
                memmove(uv_plane_new_start, uv_plane_full_start, uv_plane_real_size);
            }

            //update the size since the allocated size could still be larger than actual size
            buffer.size = y_plane_real_size + uv_plane_real_size;
#endif
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void RecorderEventCb(EventType event_type, void *event_data,
                     size_t event_data_size) {
    if (event_type == EventType::kCameraError) {
        printf("Camera error recorder event\n");
    } else if (event_type == EventType::kServerDied) {
        printf("Server died recorder event\n");
    } else {
        printf("Unknown recorder event\n");
    }
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void CameraResultCallbackHandler(uint32_t camera_id, const CameraMetadata &result)
{
    if (frame_debug) printf("%s called\n", __FUNCTION__);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void SessionEventCb(EventType event_type, void *event_data,
                    size_t event_data_size) {
    // TBD: Once support for this callback is present in QMMF
    printf("Got a Session callback\n");
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void VideoTrackDataCb(uint32_t session,
                      uint32_t track,
                      std::vector<BufferDescriptor> buffers,
                      std::vector<MetaData> meta_data) {

    if (frame_debug) printf("Got video track %u callback for session %u\n", track, session);

    int32_t buffer_index = -1;  //buffer index for accessing array of meta info

    // Cycle through each of the buffers provided
    for (auto& iter : buffers) {
        buffer_index++;

        // Publish the YUV frames to MPA
        if (track == mpa_video_track_id) {
            //remove the gap between Y and UV planes and empty space after UV planes

            if (frame_debug) {
                printf("Y plane: %s, UV plane %s\n",
                        meta_data[buffer_index].cam_buffer_meta_data.plane_info[0].ToString().c_str(),
                        meta_data[buffer_index].cam_buffer_meta_data.plane_info[1].ToString().c_str());
            }

            PackYUVImage(iter, meta_data[buffer_index]);

            camera_image_metadata_t metadata;

            metadata.magic_number = CAMERA_MAGIC_NUMBER;
            metadata.timestamp_ns = iter.timestamp;
            metadata.frame_id     = meta_data[buffer_index].cam_meta_frame_number;
            metadata.width        = meta_data[buffer_index].cam_buffer_meta_data.plane_info[0].width;
            metadata.height       = meta_data[buffer_index].cam_buffer_meta_data.plane_info[0].height;
            metadata.size_bytes   = iter.size;
            metadata.stride       = meta_data[buffer_index].cam_buffer_meta_data.plane_info[0].stride;
            metadata.exposure_ns  = 0; // Fake (unused) frame exposure_ns
            metadata.gain         = 0; // Fake (unused) frame gain
            metadata.format       = IMAGE_FORMAT_NV12;

            if (frame_debug) printf("Publishing metadata to MPA for frame %d\n", meta_data[buffer_index].cam_meta_frame_number);
            if (frame_debug) printf("Publishing frame data to MPA. %u bytes\n", metadata.size_bytes);

            //write metadata and actual image data via MPA
            pipe_server_write(1, (char*) &metadata, sizeof(camera_image_metadata_t));
            pipe_server_write(1, (char*) iter.data, metadata.size_bytes);
        }

        // Store compressed HD video frames to capture file
        if ((track == h26x_video_track_id) && (video_file)) {
            fwrite((uint8_t*) iter.data, iter.size, 1, video_file);
        }
    }

    auto ret = _recorder.ReturnTrackBuffer(session, track, buffers);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Error: VideoTrackDataCb ReturnTrackBuffer failed!: %d\n", ret);
    }
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void SnapshotCb(uint32_t camera_id, uint32_t image_sequence_count,
                BufferDescriptor buffer, MetaData meta_data) {
    if (frame_debug) printf("Got a snapshot callback for camera %u, image count %u\n", camera_id, image_sequence_count);

    std::stringstream snapshot_file_name_stream;
    snapshot_file_name_stream << snapshot_base_file_name;
    snapshot_file_name_stream << "-";
    snapshot_file_name_stream << snapshot_number;
    if (enable_yuv_snapshot) snapshot_file_name_stream << ".yuv";
    else snapshot_file_name_stream << ".jpg";
    std::string snapshot_file_name = snapshot_file_name_stream.str();

    FILE* snapshot_file = fopen(snapshot_file_name.c_str(),"wb");
    if (snapshot_file == nullptr) {
        if (frame_debug) fprintf(stderr, "Error: unable to open snapshot file %s\n", snapshot_file_name.c_str());
    } else {
        if (frame_debug) printf("Snapshot file %s opened\n", snapshot_file_name.c_str());
        if (enable_yuv_snapshot) PackYUVImage(buffer, meta_data);
        fwrite((uint8_t*) buffer.data, buffer.size, 1, snapshot_file);
        fclose(snapshot_file);
        snapshot_file = nullptr;
    }

    snapshot_number++;

    auto ret = _recorder.ReturnImageCaptureBuffer(camera_id, buffer);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Error: SanpshotCB ReturnTrackBuffer failed!: %d\n", ret);
    }
};


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void VideoTrackEventCb(uint32_t track_id, EventType event_type,
                       void *event_data, size_t event_data_size) {
    printf("Got a video track event callback for track %u\n", track_id);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void help () {
    printf("Options:\n");
    printf("-c <camera id>  Camera id. Default is 0\n");
    printf("-d              Show debug messages\n");
    printf("-f <filename>   Save h265 video to filename (No save to file by default)\n");
    printf("                For example: /sdcard/video.h265\n");
    printf("-i <filename>   Base file name for JPEG snapshots (No JPEG snapshots by default)\n");
    printf("                When set, a snapshot is taken every second. The image number is appended\n");
    printf("                to the base file name for each snapshot taken\n");
    printf("                For example: /sdcard/image\n");
    printf("                Saved images will then be named: /sdcard/image-1.jpg, /sdcard/image-2.jpg, etc.\n");
    printf("-s <resolution> Video stream resolution (640x480 by default)\n");
    printf("-r <resolution> Video capture / snapshot resolution (3840x2160 by default)\n");
    printf("-n <frame rate> Video capture frame rate (30 fps by default)\n");
    printf("-y              Set snapshots to be NV12 (YUV) (Default is JPEG)\n");
    printf("-v              Show verbose frame debugging messages (high rate)\n");
    printf("-h              Show help\n");
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
int main(int argc, char **argv) {

    int status = NO_ERRORS;

    // Local QMMF variables
    RecorderCb            recorder_status_cb;
    SessionCb             session_cb;
    TrackCb               video_track_cb;
    VideoTrackCreateParam mpa_video_track_param;
    VideoTrackCreateParam h26x_video_track_param;

    // Default MPA video track parameters
    int mpa_track_width     = 640;
    int mpa_track_height    = 480;
    int mpa_track_framerate = 30;

    // Default compressed h26x video track parameters (4k30)
    int h26x_track_width     = 3840;
    int h26x_track_height    = 2160;
    int h26x_track_framerate = 30;

    // Snapshot parameters
    ImageParam snapshot_param;
    std::vector<int> snapshot_meta;
#ifdef QRB5165
    ImageConfigParam snapshot_extra_param;
#endif

    // parse input args
    int opt;

    // put ':' in the starting of the
    // string so that program can
    //distinguish between '?' and ':'
    while ((opt = getopt(argc, argv, ":c:n:f:yi:r:s:dvh")) != -1) {
        switch (opt) {
        case 'c':
            camera_number = atoi(optarg);
            printf("Setting camera id: %d\n", camera_number);
            break;
        case 'n':
            h26x_track_framerate = atoi(optarg);
            printf("Setting h26x capture frame rate: %d\n", h26x_track_framerate);
            break;
        case 'f':
            video_out_file_name = optarg;
            printf("Setting video filename: %s\n", video_out_file_name.c_str());
            break;
        case 'y':
            enable_yuv_snapshot = true;
            break;
        case 'i':
            snapshot_base_file_name = optarg;
            printf("Setting snapshot base filename: %s\n", snapshot_base_file_name.c_str());
            break;
        case 's':
        {
            char* w = strtok(optarg, "x");
            char* h = strtok(NULL, "x");
            if(w == NULL || h == NULL) {
                fprintf(stderr, "Error - incorrect resolution format: WIDTHxHEIGHT\n");
                help();
                return ERROR;
            } else {
                mpa_track_width = atoi(w);
                mpa_track_height = atoi(h);
            }
            break;
        }
        case 'r':
        {
            char* w = strtok(optarg, "x");
            char* h = strtok(NULL, "x");
            if(w == NULL || h == NULL) {
                fprintf(stderr, "Error - incorrect resolution format: WIDTHxHEIGHT\n");
                help();
                return ERROR;
            } else {
                h26x_track_width = atoi(w);
                h26x_track_height = atoi(h);
            }
            break;
        }
        case 'd':
            debug = true;
            break;
        case 'v':
            frame_debug = true;
            break;
        case 'h':
            help();
            return NO_ERRORS;
        case ':':
            fprintf(stderr, "Error - option needs a value\n\n");
            help();
            return ERROR;
        case '?':
            fprintf(stderr, "Error - unknown option: %c\n\n", optopt);
            help();
            return ERROR;
        default:
            help();
            return ERROR;
        }
    }

    // optind is for the extra arguments
    // which are not parsed
    for (; optind < argc; optind++) {
        fprintf(stderr, "extra arguments: %s\n", argv[optind]);
        help();
        return ERROR;
    }

    if (debug) {
        printf("Camera configuration:\n");
        printf("\tCamera id: %d\n", camera_number);
        printf("\tStreaming resolution: %dx%d\n", mpa_track_width, mpa_track_height);
        printf("\tStreaming frame rate: %d fps\n", mpa_track_framerate);
        if (video_out_file_name.empty()) {
            printf("\tFile capture disabled\n");
        } else {
            printf("\tCapture filename: %s\n", video_out_file_name.c_str());
            printf("\tCapture resolution: %dx%d\n", h26x_track_width, h26x_track_height);
            printf("\tCapture frame rate: %d fps\n", h26x_track_framerate);
        }
        if (snapshot_base_file_name.empty()) {
            printf("\tJPEG snapshot disabled\n");
        } else {
            printf("\tSnapshot base filename: %s\n", snapshot_base_file_name.c_str());
            printf("\tSnapshot resolution: %dx%d\n", h26x_track_width, h26x_track_height);
            printf("\tSnapshot frame rate: %d fps\n", 1);
            if (enable_yuv_snapshot) printf("\tSnapshot format is NV12 (YUV)\n");
            else printf("\tSnapshot format is JPEG\n");
        }
    }

    // Set these up after processing the options
    snapshot_param.width = h26x_track_width;
    snapshot_param.height = h26x_track_height;
    snapshot_param.image_quality = 99;  // Alex K says this parameter is ignored?
    if (enable_yuv_snapshot) snapshot_param.image_format = ImageFormat::kNV12;
    else snapshot_param.image_format = ImageFormat::kJPEG;

    // MPA pipe configuration
    int server_fifo_size = (sizeof(camera_image_metadata_t) + (mpa_track_width * mpa_track_height)) * 3;
    pipe_info_t camera_pipe_info =
    {
        "hires",                      // pipe name
        "",                           // pipe location
        "camera_image_metadata_t",	  // pipe type
        "voxl-hires-server",	      // server_name
        server_fifo_size,	          // FIFO size in bytes
        0							  // optioinal server_pid
    };

    // Connect
    auto ret = _recorder.Connect(recorder_status_cb);
    recorder_status_cb.event_cb = [&] (EventType event_type, void *event_data,
                                       size_t event_data_size)
                                      { RecorderEventCb(event_type, event_data,
                                                        event_data_size); };
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Error: unable to connect to recorder service: %d\n", ret);
        return ERROR;
    } else {
        if (debug) printf("Connected\n");
    }

    CameraResultCb result_cb = [&] (uint32_t camera_id,
        const CameraMetadata &result) { CameraResultCallbackHandler(camera_id, result); };

    // Start camera parameters
#ifdef QRB5165
    // struct TrackCrop crop_param;
    // crop_param.width = 640;
    // crop_param.height = 480;
    CameraExtraParam extra_param;
    // extra_param.Update(QMMF_TRACK_CROP, crop_param);
    ret = _recorder.StartCamera(camera_number, 30.0f, extra_param, result_cb);
#else
    struct CameraStartParam csp;
    csp.zsl_mode                = false; //disable zero shutter lag mode (not used)
    csp.enable_partial_metadata = false; //unused?
    csp.zsl_queue_depth         = 8;     //unused
    csp.zsl_width               = 3840;  //unused
    csp.zsl_height              = 2160;  //unused
    csp.frame_rate              = 30;    //unused? since frame rate is determined by track frame rate
    csp.flags                    = 0;

    // Start camera
    ret = _recorder.StartCamera(camera_number, csp, result_cb);
#endif
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Error: unable to start camera. Start return value %d\n", ret);
        fprintf(stderr, "       Please verify camera id\n");
        status = ERROR;
        goto disconnect;
    } else {
        if (debug) printf("Started camera %d\n", camera_number);
    }

    // Create session
    session_cb.event_cb = [&] (EventType event_type, void *event_data,
                               size_t event_data_size)
                              { SessionEventCb(event_type, event_data,
                                               event_data_size); };
    ret = _recorder.CreateSession(session_cb, &session_id);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Error: unable to connect to create session: %d\n", ret);
        status = ERROR;
        goto stopcamera;
    } else {
        if (debug) printf("Created session\n");
    }

    // Create MPA video track
    memset(&mpa_video_track_param, 0x0, sizeof mpa_video_track_param);

    mpa_video_track_param.camera_id      = camera_number;
    mpa_video_track_param.width          = mpa_track_width;
    mpa_video_track_param.height         = mpa_track_height;
    mpa_video_track_param.frame_rate     = mpa_track_framerate;
#ifndef QRB5165
    mpa_video_track_param.low_power_mode = true;
    mpa_video_track_param.format_type    = VideoFormat::kYUV;
#else
    mpa_video_track_param.format_type    = VideoFormat::kNV12;
#endif
    video_track_cb.data_cb = [&] (uint32_t track_id,
                                  std::vector<BufferDescriptor> buffers,
                                  std::vector<MetaData> meta_data)
                                 { VideoTrackDataCb(session_id, track_id,
                                                    buffers, meta_data); };
    video_track_cb.event_cb = [&] (uint32_t track_id,
                                   EventType event_type,
                                   void *event_data,
                                   size_t event_data_size)
                                  { VideoTrackEventCb(track_id, event_type,
                                                      event_data,
                                                      event_data_size); };

    ret = _recorder.CreateVideoTrack(session_id, mpa_video_track_id, mpa_video_track_param, video_track_cb);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Error: unable to connect to create MPA video track: %d\n", ret);
        status = ERROR;
        goto deletesession;
    } else {
        if (debug) printf("MPA video track created\n");
    }

    // If a filename has been configured to capture video then create / open
    // the file now.
    if ( ! video_out_file_name.empty()) {
        video_file = fopen(video_out_file_name.c_str(),"wb");
        if (video_file == nullptr) {
            fprintf(stderr, "Error: unable to open video capture file\n");
            status = ERROR;
            goto deletempatrack;
        } else {
            if (debug) printf("Video capture file opened\n");
        }
    }

    // Create h26x video track if there is a capture file open
    if (video_file) {
        memset(&h26x_video_track_param, 0x0, sizeof h26x_video_track_param);

        h26x_video_track_param.camera_id      = camera_number;
        h26x_video_track_param.width          = h26x_track_width;
        h26x_video_track_param.height         = h26x_track_height;
        h26x_video_track_param.frame_rate     = h26x_track_framerate;
#ifndef QRB5165
        h26x_video_track_param.low_power_mode = true;
#endif
        h26x_video_track_param.format_type    = VideoFormat::kHEVC;

        // h265 codec params
        // TODO: Tune this for good video quality of saved video
        h26x_video_track_param.codec_param.hevc.idr_interval = 1;
        h26x_video_track_param.codec_param.hevc.bitrate = 5000000;
        h26x_video_track_param.codec_param.hevc.profile = HEVCProfileType::kMain;
        h26x_video_track_param.codec_param.hevc.level = HEVCLevelType::kLevel5_1;
        h26x_video_track_param.codec_param.hevc.ratecontrol_type = VideoRateControlType::kMaxBitrate;
        h26x_video_track_param.codec_param.hevc.qp_params.enable_init_qp = true;
        h26x_video_track_param.codec_param.hevc.qp_params.init_qp.init_IQP = 27;
        h26x_video_track_param.codec_param.hevc.qp_params.init_qp.init_PQP = 28;
        h26x_video_track_param.codec_param.hevc.qp_params.init_qp.init_BQP = 28;
        h26x_video_track_param.codec_param.hevc.qp_params.init_qp.init_QP_mode = 0x7;
        h26x_video_track_param.codec_param.hevc.qp_params.enable_qp_range = true;
        h26x_video_track_param.codec_param.hevc.qp_params.qp_range.min_QP = 10;
        h26x_video_track_param.codec_param.hevc.qp_params.qp_range.max_QP = 51;
        h26x_video_track_param.codec_param.hevc.qp_params.enable_qp_IBP_range = true;
        h26x_video_track_param.codec_param.hevc.qp_params.qp_IBP_range.min_IQP = 10;
        h26x_video_track_param.codec_param.hevc.qp_params.qp_IBP_range.max_IQP = 51;
        h26x_video_track_param.codec_param.hevc.qp_params.qp_IBP_range.min_PQP = 10;
        h26x_video_track_param.codec_param.hevc.qp_params.qp_IBP_range.max_PQP = 51;
        h26x_video_track_param.codec_param.hevc.qp_params.qp_IBP_range.min_BQP = 10;
        h26x_video_track_param.codec_param.hevc.qp_params.qp_IBP_range.max_BQP = 51;
        h26x_video_track_param.codec_param.hevc.ltr_count = 0;
        h26x_video_track_param.codec_param.hevc.insert_aud_delimiter = false;
        h26x_video_track_param.codec_param.hevc.hier_layer = 0;
        h26x_video_track_param.codec_param.hevc.prepend_sps_pps_to_idr = false;
        h26x_video_track_param.codec_param.hevc.sar_enabled = false;
        h26x_video_track_param.codec_param.hevc.sar_width = 0;
        h26x_video_track_param.codec_param.hevc.sar_height = 0;

        ret = _recorder.CreateVideoTrack(session_id, h26x_video_track_id, h26x_video_track_param, video_track_cb);
        if (ret != NO_ERRORS) {
            fprintf(stderr, "Error: unable to connect to create h26x video track: %d\n", ret);
            status = ERROR;
            goto closevideofile;
        } else {
            if (debug) printf("h26x video track created\n");
        }
    }

    // Create MPA server pipe
    if (pipe_server_create(1, camera_pipe_info, 0)) {
        fprintf(stderr, "Error: could not create camera pipe.");
        goto deleteh26xtrack;
    } else {
        if (debug) printf("MPA server pipe created\n");
    }

    // Start session
    ret = _recorder.StartSession(session_id);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Error: unable to connect to start session: %d\n", ret);
        status = ERROR;
        goto closepipe;
    } else {
        if (debug) printf("Started session\n");
    }

#ifdef QRB5165
    ret = _recorder.ConfigImageCapture(camera_number, snapshot_param, snapshot_extra_param);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Error: unable to configure image capture: %d\n", ret);
        status = ERROR;
        goto stopsession;
    } else {
        if (debug) printf("Configured image capture\n");
    }
#endif

    // Run until commanded to shutdown.
    printf("Ctrl-c or 'kill %d' to end\n", getpid());
    signal(SIGHUP, signal_handler);
    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);
    while (running){
        if ( ! snapshot_base_file_name.empty()) {
#ifdef QRB5165
            ret = _recorder.CaptureImage(camera_number, 1,
                                         (std::vector<CameraMetadata>&) snapshot_meta,
                                         SnapshotCb);
#else
            ret = _recorder.CaptureImage(camera_number, snapshot_param, 1,
                                         (std::vector<CameraMetadata>&) snapshot_meta,
                                         SnapshotCb);
#endif
            if (ret != NO_ERRORS) {
                fprintf(stderr, "Error: unable to trigger snapshot: %d\n", ret);
            } else {
                if (frame_debug) printf("Triggered snapshot\n");
            }
        }

        sleep(1);
    }

    // Probably no need for this but better to be safe.
    if ( ! snapshot_base_file_name.empty()) {
        (void) _recorder.CancelCaptureImage(camera_number);
    }

stopsession:
    ret = _recorder.StopSession(session_id, true);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Error: unable to connect to stop session: %d\n", ret);
        return ERROR;
    } else {
        if (debug) printf("Stopped session\n");
    }

closepipe:
    pipe_server_close(1);
    if (debug) printf("Closing MPA pipe\n");

deleteh26xtrack:
    if (video_file) {
        ret = _recorder.DeleteVideoTrack(session_id, h26x_video_track_id);
        if (ret != NO_ERRORS) {
            fprintf(stderr, "Error: unable to delete h26x video track: %d\n", ret);
            return ERROR;
        } else {
            if (debug) printf("h26x video track deleted\n");
        }
    }

closevideofile:
    if (video_file) {
        fclose(video_file);
        video_file = nullptr;
        if (debug) printf("Video file closed\n");
    }

deletempatrack:
    ret = _recorder.DeleteVideoTrack(session_id, mpa_video_track_id);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Error: unable to delete MPA video track: %d\n", ret);
        return ERROR;
    } else {
        if (debug) printf("MPA video track deleted\n");
    }

deletesession:
    ret = _recorder.DeleteSession(session_id);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Error: unable to delete session: %d\n", ret);
        return ERROR;
    } else {
        if (debug) printf("Session deleted\n");
    }

stopcamera:
    ret = _recorder.StopCamera(camera_number);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Error: unable to connect to stop camera: %d\n", ret);
        return ERROR;
    } else {
        if (debug) printf("Stopped Camera\n");
    }

disconnect:
    ret = _recorder.Disconnect();
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Error: unable to disconnect from recorder service: %d\n", ret);
        return ERROR;
    } else {
        if (debug) printf("Disconnected\n");
    }

    return NO_ERRORS;
}
